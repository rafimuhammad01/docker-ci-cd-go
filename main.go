package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
)

func main() {

	http.HandleFunc("/", HelloHandler)

	fmt.Println("Server started at port 8080")
	log.Fatal(http.ListenAndServe(":"+os.Getenv("PORT"), nil))
}

func HelloHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]interface{}{
		"message": "hello world from docker ci-cd!",
	})
}
